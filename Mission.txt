
We mutually benefit by sharing physical facility and tools,
information, education, and assistence.  We relentlessly expand our
goals and welcome new members, who will find all instincts towards
parasitism crushed, all instincts towards mutalism encouraged, and be
relentlessly pressured to make their own goals more ambitious.

Individually we work on side projects, small business experiments,
growing small businesses larger, free form research and
experimentation, education and experimentation, to better ourselves.
